## Description

**Issues** :

## Documentation

- [ ] Upgrade the version : `_coverpage` and `package.json`
- [ ] Add entry to the changelog
- [ ] Update documentation if needed

## Generator

- [ ] New or updated files?

## Checklist before merge :
- [ ] No remaining `console.log` 
- [ ] Version changed if new feature
- [ ] Lint ok
- [ ] all eslintConfig rules are on ("2") (except `no-useless-escape` / `vue/no-mutating-props`)
- [ ] Try `npm run build:app`
- [ ] Try `npm run build:admin`

## Tests
- [ ] Admin tests ok ?
- [ ] Add new unit test or e2e test