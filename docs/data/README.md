# Les différents types de données

Ceci est une documentation technique du format dans lequel les données sont écrites pour être compréhensibles par le mémo.

* Les [cartes](/data/cards/)
* Les [liens entre cartes](/data/links/)
* Les [fresques (corrections)](/data/fresks/)
* La [configuration du mémo](/data/settings)