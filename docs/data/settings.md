# Le fichier de configuraton

**Chemin** : `settings.json`

**Schéma** : `data-validation/schemas/settings.json`

Ce fichier `json` permet de configurer toutes les informations générales du mémo.