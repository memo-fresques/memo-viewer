# Les fonctionnalités du mémo

## Affichage des cartes et des liens

Le mémo permet de voir, pour chaque carte du jeu :
* la carte (recto et verso) ;
* les explications complémentaires sur cette carte ;
* quelles autres cartes sont reliées à cette carte et avoir des explications sur la cause de ce lien.

Les explications complémentaires sont fournies en format markdown, ce qui offre une très grande liberté de mise en page (ajout de liens, mise en valeur de texte en gras, ajout de photos, etc.)


## Affichage de la correction

On peut visualiser toutes les cartes ainsi que les liens entre celles-ci (avec des flèches). Cette correction peut être affichée lot par lot.

On peut faire basculer cette correction en mode quiz : au lieu des cartes, on voit des points d’interogation et celles-ci se dévoilent en double-cliquant sur les points d’interrogation.

## 💬Internationalisation et multi-langue

Le mémo peut être affiché dans n'importe quelle langue. La langue est détectée automatiquement (en se basant sur les réglages du navigateur de l’utilisateurice) et peut être modifiée par un menu dédié. Lorsqu'une traduction est manquante, le texte est affiché en anglais (ou dans une langue dans laquelle il est disponible).

## 📱 Pensé pour le smartphone

Dès sa conception, le site a été pensé pour être adapté au mobile. Les affichages des cartes y sont optimisées et certaines fonctionnalités sont uniquement accessibles sur téléphone, comme la possibilité de passer d’une carte à l’autre en _glissant_ sont doigt de droite à gauche.

## 📝 Modifiable et contributif

Les données sont écrites dans des formats compréhensibles pour l’humain et la machine (en l'occurence du Markdown). En s’aidant de la [documentation](/data) on peut modifier ou ajouter des cartes ou des liens inter-cartes.

Et ces données sont modifiables facilement grâce à une interface d'administration dédiée : [voir la documentation](/admin/).

## Interface d'administration

Pour que chaque Fresque soit complètement autonome dans la mise à jour régulière de ses données.