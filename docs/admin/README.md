# Administration

Le mémo propose depuis la version 1.5 une interface d'administration 100% faite maison pour modifier, ajouter et supprimer les explications et traductions des cartes ou des liens entre les cartes. Cette interface est évolutive et a pour vocation de répondre à l’essentiel des demandes courantes. Pour des cas plus particuliers, il faudra aller directement modifier le code source.

Pour y accéder, il suffit d'ajouter `/admin` à l'url de votre mémo.

Par exemple : https://memo.climatefresk.org/admin

* 👉 [Comment utiliser l'interface d'administration](/admin/cms)
* 👉 [Écrire les explications en Markdown](/admin/markdown)
* 👉 [Statistiques de visites](/admin/matomo)
* 👉 [Modifier les images des cartes](/admin/cards)
* 👉 [Modifier les Fresques](/admin/fresks)
* 👉 [L'interface d'admininistration pour les développeur·euses](/admin/dev)