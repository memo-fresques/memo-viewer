# Déployer une nouvelle instance

Vous souhaitez créer et déployer une nouvelle instance du mémo pour votre Fresque ? Vous trouverez ici toutes les informations pour y arriver !

* [Pré-requis](/deploy/prerequisites)
* [Comment créer son instance à partir de zéro ?](/deploy/yo-generator)
* [La déployer sur son serveur](/deploy/server)

Vous pouvez gérer les traductions de vos données (explications des cartes) avec Tolgee, comment faire ? [Gérer les traductions avec Tolgee](/deploy/tolgee)