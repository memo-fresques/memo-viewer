# Pré-requis


?> Avant toute chose, il est important de prendre un temps pour comprendre ce qu'est le mémo et si cet outil peut ou non répondre à vos besoins (ou s'il est nécessaire de faire des nouveaux développements dans l'outil pour cela).

**Vous êtes décidé·e à créer un mémo ?**
Voici les éléments à disposer pour avoir un mémo pleinement opérationnel !

### 🖼️ Une nécessité : les cartes en format pdf

_A priori_, vous l'avez déjà, il s'agit du fichier pdf que vous utilisez pour impression : ce n'est pas gênant si ce fichier contient des pages en plus, si les cartes ne sont pas dans l'ordre ou si le pdf contient des hirondelles (marges pour l'impression) : le mémo est capable de le gérer.


### 🎨 Des indispensables pour personnaliser le mémo

Pour personnaliser le mémo, vous avez besoin de rassembler :
- Le **logo de la Fresque** qui sera affiché en haut à gauche du mémo. Il est possible d’avoir un logo différent par langue ;
- Les **deux couleurs principales et secondaires** à afficher ;
- Le **favicon**, petite image illustration qui est affichée en haut du navigateur ;
- Le **Texte de description de la Fresque** pour la page _À propos_ ;

### 📚 Pour donner de la matière aux utilisateur·ices : toutes les explications

L'intérêt du mémo repose sur le contenu que vous y mettrez, est-ce du contenu que vous avez déjà dans un autre support (par ex. un guide de formation) ? Est-ce que vous avez besoin de rédiger ces documents ?

Il est conseillé d'attendre la mise en place de l'interface d'administration (en même temps que le mémo) pour renseigner toutes les explications. Mais si vous disposez déjà de ces informations dans un format structuré (fichier excel, base de données) alors des migrations automatiques peuvent être faites par des développeur·euses.

Les informations que vous pourrez renseigner sur le mémo :
- Pour chaque carte : son titre et des explications sur cette carte ;
- Les liens cause-conséquence entre les cartes et des explications sur ces liens ;
- Créer les vues _corrections_ en positionnant les différentes cartes.
