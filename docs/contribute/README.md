# Contribuer

Parce que que c’est un logiciel libre, le mémo est un bien commun à disposition de tous·te·s. N’importe qui est invité·e à contribuer à sa manière à faire évoluer l'outil.

Quelques manières de contribuer :
* aider à définer et concevoir les futures fonctionnalités : quels sont les besoins des Fresques ?
* traduire le mémo dans sa langue ;
* proposer des pistes d’amélioration de l’expérience utilisateurice, du design ;
* développer de nouvelles fonctionnalités ;
* accompagner de nouvelles Fresques dans la mise en place du mémo ;
* parler du mémo autour de vous ;
* aider à rendre le mémo plus accessible ;
* aider à diminuer l'impact environnemental du mémo

Le projet Framagit : [framagit.org/memo-fresques/memo-viewer](https://framagit.org/memo-fresques/memo-viewer) <br/>
Mail de contact : dev@marc-antoinea.fr

## Contributeur·ices

* [Julien Robberechts](https://julienrobberechts.github.io/) : conception et développement initial du mémo. Contributeur principal jusqu'en janvier 2022.
* [Marc-AntoineA](https://marc-antoinea.fr) : développeur depuis le printemps 2021, contributeur principal depuis janvier 2022
* Ollivier Bonnet : traduction allemande du mémo (hors interface d’administration)
* Fresque du Climat : soutien financier pour le support « multi-versions » (version 1.7.0)