export interface CardGridItemType {
    id: string;
    image: string;
    num: string | number;
    title: string;
}
