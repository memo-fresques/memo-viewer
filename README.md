# Memo-viewer


Le mémo est le site internet pour donner les clés aux animateur·ices de la [Fresque du Climat](https://fresqueduclimat.org/) et avoir des explications détaillées sur chacune des cartes. On peut y accéder à l’url [memo.climatefresk.org/](https://memo.climatefresk.org).

### 👉 **En savoir plus : [memo.fresque.earth](https://memo.fresque.earth)** 👈
_Être au courant des dernières fonctionnalités sur Télégram : https://t.me/+EB9mGj24fE5kNDdk_

Ce logiciel peut être utilisé pour visualiser n'importe quelle fresque !


**Vous souhaitez déployer le mémo pour votre fresque ?**
Le logiciel est sous license libre [GPLv3](https://framagit.org/memo-fresques/memo-viewer/-/blob/main/LICENSE). Vous pouvez en particulier utiliser le mémo comme bon vous semble à condition que votre logiciel soit lui même sous license GPLv3.<br/>
**Contact** : dev@marc-antoinea.fr
