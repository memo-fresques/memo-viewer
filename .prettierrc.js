module.exports = {
    singleQuote: true,
    semi: true,
    tabWidth: 4,
    bracketSameLine: true,
    arrowParens: 'always',
    vueIndentScriptAndStyle: false,
    printWidth: 150,
    bracketSpacing: true,
};
