---
title: 'Bilan énergétique'
backDescription: 'Ce graphique explique où va l’énergie qui s’accumule sur la terre à cause du forçage radiatif : elle réchauffe l’océan, fait fondre la glace, se dissipe dans le sol et réchauffe l’atmosphère.'
lot: '3'
num: 14
---
Contributon minimale