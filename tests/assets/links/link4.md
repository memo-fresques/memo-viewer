---
fromCardId: '20'
toCardId: '3'
status: invalid
class: hell33e
---
Les médias détenus par les groupes industriels sont amenés à promouvoir l'idée d'une fiscalité supposément "confiscatoire" à l'égard des plus riches, notamment en mettant en avant les prélèvements obligatoires et en les comparant à des pays qui n'offrent pas le même niveau de prestations sociales et de services publics. Ils peuvent aussi promouvoir des théories non démontrées comme celle du ruissèlement, selon laquelle l'Etat devrait permettre l'enrichissement des personnes les plus riches en abaissant leurs impôts, afin qu'elles réinjectent cette richesse dans l'économie pour créer de l'activité. 
